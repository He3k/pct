#include <iostream>
#include <string>
#include <vector>
//#include <openssl/md5.h>
#include <openssl/sha.h>
#include <cstring>
#include <mpi.h>
#include <unistd.h>
#include <cmath>

std::vector<char> alphabet({ 'a', 'b', 'c', 'q', 'w', 'e', 'r', 't', 'y', 'u'});
int length = 10;
unsigned char hash[SHA_DIGEST_LENGTH];
int collision = 0;

void getStartHash()
{
    SHA_CTX sha1handler;
    char buffer[length] = "qcqacwetyu";

    SHA1_Init(&sha1handler);
    SHA1_Update(&sha1handler, buffer, length);
    SHA1_Final(hash, &sha1handler);
}

int getHash(std::string password)
{
    SHA_CTX sha1handler;
    unsigned char sha1digets[SHA_DIGEST_LENGTH];
    const char *buffer = password.c_str();

    SHA1_Init(&sha1handler);
    SHA1_Update(&sha1handler, buffer, length);
    SHA1_Final(sha1digets, &sha1handler);

    if (strncmp((char*)hash, (char*)sha1digets, length) == 0)
        return 1;
    else
        return 0;
}
 
std::string generatorPassword(std::vector<char> alphabet, int idx, int size)
{
    std::string password(size, alphabet[0]);
    int alphas = alphabet.size();

    while (size--) {
        password[size] = alphabet[idx % alphas];
        idx /= alphas;
    }

    return password;
}

void hackingHash(int size, std::vector<char> alphabet)
{
    int commsize, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int c = 0;

    size_t numbers = 1;
    int alphas = alphabet.size();
    numbers = pow(alphas, size);

    size_t numbers_in_proc = numbers / commsize;
    size_t lb = rank * numbers_in_proc;
    size_t ub = (rank == commsize - 1) ? (numbers - 1) : (lb+numbers_in_proc - 1);

    for (size_t i = lb; i <= ub; ++i) {
        std::string str = generatorPassword(alphabet, i, size);
        c += getHash(str);
    }

    MPI_Reduce(&c, &collision, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
}

int main(int argc, char* argv[])
{
    int commsize, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    getStartHash();

    double t = MPI_Wtime();
    hackingHash(length, alphabet);
    t = MPI_Wtime() - t;

    if (rank == 0)  {
        printf("Time (%d procs): %.6f sec.\n", commsize, t);
        printf("Count collision: %d\n", collision);
    }

    MPI_Finalize();
}
