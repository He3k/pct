from sys import argv
from functools import reduce
import itertools
from hashlib import sha1
from mpi4py import MPI
import time

#kolvo = 5
#hash = aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d

try:
	kolvo,hash = int(argv[1]),argv[2]
except IndexError:
    print("Error: Arguments!")
    print("./brosha1py <count_symbols> <hash>")
    raise SystemExit

def encrypt(word_string, hash):
    start_time = time.time()
    passwd = word_string.encode()
    signature = sha1(passwd).hexdigest()
    if signature == hash:
        print (signature, word_string)
        #print("---%s seconds ---" % (time.time() - start_time))
        MPI.COMM_WORLD.Abort()

def generator(kolvo, hash):
    size = comm.Get_size()
    rank = comm.Get_rank()
    seed = "QWERTYUIOPASDFGHJKLZXCVBNMketaonhisrdlumfcgwypbvxjqz" + "1234567890"
    seed_bytes = list(map(ord, seed))
    numbers = int(pow(len(seed),kolvo))
    num_in_proc = int(numbers / size)
    lb = int(rank * num_in_proc)
    #ub = int((rank == size-1) ? (numbers - 1) : (lb+num_in_proc - 1))
    if (rank == size - 1):
        ub = int(numbers - 1)
    else:
        ub = int(lb + num_in_proc - 1)
    #print(numbers)  
    #print(len(seed))
    for word_bytes in itertools.product(seed_bytes, repeat=kolvo):
        word_string = reduce(lambda x, y: x+y, map(chr, word_bytes))
        encrypt(word_string, hash)

#MPI.Init()
print("Computing ...")
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
start_time = MPI.Wtime()
generator(kolvo, hash)
end_time = MPI.Wtime()
if rank == 0:
    print ("Time:" + str(end_time-start_time), size)
    #print ("Count collision:")
MPI_Finalize()
