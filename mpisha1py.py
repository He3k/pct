from sys import argv
from functools import reduce
import itertools
from hashlib import sha1
from mpi4py import MPI
import time

#kolvo = 5
#hash = aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d

try:
	kolvo,hash = int(argv[1]),argv[2]
except IndexError:
    print("Error: Arguments!")
    print("./brosha1py <count_symbols> <hash>")
    raise SystemExit

def encrypt(word_string, hash):
    start_time = time.time()
    passwd = word_string.encode()
    signature = sha1(passwd).hexdigest()
    if signature == hash:
        print (signature, word_string)
        print("---%s seconds ---" % (time.time() - start_time))
        MPI.COMM_WORLD.Abort()

def generator(kolvo, hash):
    seed = "QWERTYUIOPASDFGHJKLZXCVBNMketaonhisrdlumfcgwypbvxjqz" + "1234567890"
    seed_bytes = list(map(ord, seed))
    for word_bytes in itertools.product(seed_bytes, repeat=kolvo):
        word_string = reduce(lambda x, y: x+y, map(chr, word_bytes))
        encrypt(word_string, hash)

print("Computing ...")
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
generator(kolvo, hash)
